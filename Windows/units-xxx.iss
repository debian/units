; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

; for existing installation of 32-bit units
#define MyOldAppId "{{2F402833-521E-46CF-A761-B9FE0708D7B8}"
#define MyOldUninstallPath "SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\"

#define MyAppName "units"
#define MyAppName2 "units WT"
#define MyAppId "{{05363962-145B-4B39-890B-11049DD1EBE3}"
#define MyUninstallPath "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\"
#define MyAppVersion "xxx"
#define MyAppPublisher "Free Software Foundation, Inc."
#define MyAppURL "http://www.gnu.org/software/units/"
#define MyAppExeName "units.exe"
#define MyPython "python.exe"
#define MyAppCurrencyFile "currency.units"
#define MyAppCPIFile "cpi.units"
#define MyAppElementsFile "elements.units"
#define MyAppCurrencyUpdater "units_cur.py"
#define MyAppCurrencyUpdaterName "Currency updater"
#define MyAppWindowsManual "UnitsForWindows.pdf"
#define MyAppWindowsManualName "Units for Windows"
#define MyAppManual "units.pdf"
#define MyAppManualName "Units User Manual"
#define MyWindowsConsoleHost "{sys}\conhost.exe"
#define MyWindowsConsoleHostParams "cmd /c title units"
#define MyWindowsTerminal "wt.exe"
#define MyWindowsTerminalParams "cmd /c title units && chcp 65001 > NUL"
#define MyWindowsTerminalRegKey "SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\wt.exe"
#define SourceDir "."
   
[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
; 64 bit version, units 2.20 and later
; AppId={{05363962-145B-4B39-890B-11049DD1EBE3}
AppId={#MyAppId}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
ArchitecturesInstallIn64BitMode=x64compatible
ArchitecturesAllowed=x64compatible
DefaultDirName={commonpf}\GNU\units
DefaultGroupName={#MyAppName}
LicenseFile={#SourceDir}\License.txt
OutputDir=.
OutputBaseFilename=units-{#MyAppVersion}-setup
PrivilegesRequired=admin
UsedUserAreasWarning=no
SetupIconFile={#SourceDir}\gnu.ico
AllowNoIcons=yes
ChangesAssociations=yes
ChangesEnvironment=yes
Compression=lzma
SolidCompression=yes
WizardStyle=modern

[InstallDelete]


[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[CustomMessages]
AddToPath=Add %1 directory to PATH
ViewReadme=View the Readme file
ViewUnitsForWindows=View Units for Windows (PDF)
ViewManual=View the units manual (PDF)

[Tasks]
Name: "desktopicon"; Description: "Create a  desktop shortcut"
Name: "addtopath"; Description: "{cm:AddToPath,{#MyAppName}}"
Name: "fileassociation"; Description: "Associate *.units files with Notepad"

[Files]
Source: "{#SourceDir}\units.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#SourceDir}\{#MyAppCurrencyUpdater}"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#SourceDir}\License.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#SourceDir}\Readme.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#SourceDir}\currency.units"; DestDir: "{app}"; Permissions: users-modify powerusers-modify; Flags: ignoreversion
Source: "{#SourceDir}\cpi.units"; DestDir: "{app}"; Permissions: users-modify powerusers-modify; Flags: ignoreversion
Source: "{#SourceDir}\elements.units"; DestDir: "{app}"; Permissions: users-modify powerusers-modify; Flags: ignoreversion
Source: "{#SourceDir}\definitions.units"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#SourceDir}\locale_map.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#SourceDir}\UnitsForWindows.pdf"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#SourceDir}\units.pdf"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#SourceDir}\unitsprog.ico"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#SourceDir}\unitsfile.ico"; DestDir: "{app}"; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
; Start Menu shortcuts
Name: "{group}\{#MyAppName}"; Filename: "{#MyWindowsConsoleHost}"; \
      Parameters: "{#MyWindowsConsoleHostParams} && ""{app}\{#MyAppExeName}"""; IconFilename: "{app}\unitsprog.ico"
Name: "{group}\{#MyAppName2}"; Filename: "{#MyWindowsTerminal}"; \
      Parameters: "{#MyWindowsTerminalParams} && ""{app}\{#MyAppExeName}"""; \
      IconFilename: "{app}\unitsprog.ico"; Check: MyCreateWindowsTerminalShortcut
Name: "{group}\{#MyAppCurrencyUpdaterName}"; Filename: "{app}\{#MyAppCurrencyUpdater}"; \
      WorkingDir: "{%UserProfile}"
Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppName}}"; Filename: "{#MyAppURL}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{group}\{#MyAppWindowsManualName}"; Filename: "{app}\{#MyAppWindowsManual}"
Name: "{group}\{#MyAppManualName}"; Filename: "{app}\{#MyAppManual}";
; desktop shortcuts
Name: "{commondesktop}\{#MyAppName}"; Filename: "{#MyWindowsConsoleHost}"; \
      Parameters: "{#MyWindowsConsoleHostParams} && ""{app}\{#MyAppExeName}"""; IconFilename: "{app}\unitsprog.ico"; \
      Tasks: desktopicon; WorkingDir: "{%UserProfile}"
Name: "{commondesktop}\{#MyAppName2}"; Filename: "{#MyWindowsTerminal}"; \
      Parameters: "{#MyWindowsTerminalParams} && ""{app}\{#MyAppExeName}"""; \
      IconFilename: "{app}\unitsprog.ico"; Tasks: desktopicon;  \
      WorkingDir: "{%UserProfile}"; Check: MyCreateWindowsTerminalShortcut; 
     
[Run]
Filename: "{app}\Readme.txt"; Description: "{cm:ViewReadme}"; \
          Flags: waituntilterminated postinstall skipifsilent shellexec
Filename: "{app}\UnitsForWindows.pdf"; Description: "{cm:ViewUnitsForWindows}"; \
          Flags: nowait postinstall skipifsilent shellexec
Filename: "{app}\units.pdf"; Description: "{cm:ViewManual}"; Flags: nowait postinstall skipifsilent shellexec

Filename: "{#MyWindowsConsoleHost}"; Parameters: "{#MyWindowsConsoleHostParams} && ""{app}\{#MyAppExeName}"""; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; \
          Flags: nowait postinstall skipifsilent


[Registry]
; Don't add units directory to PATH if it's already there
; All users
;Root: "HKLM"; Subkey: "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"; ValueType: expandsz; ValueName: "PATH"; ValueData: "{olddata};{app}"; Check: NeedsAddPath(ExpandConstant('{app}')); Tasks: addtopath
; Current user only
Root: "HKCU"; Subkey: "Environment"; ValueType: expandsz; ValueName: "PATH"; \
      ValueData: "{olddata};{app}"; Check: NeedsAddPath(ExpandConstant('{app}')); Tasks: addtopath

; Associate ".units" with notepad and units icon
Root: "HKCR"; Subkey: ".units"; ValueType: string; ValueData: "Units.DataFile";
Root: "HKCR"; Subkey: "Units.DataFile"; ValueType: string; ValueData: "GNU units data file"; \
      Flags: uninsdeletekeyifempty
Root: "HKCR"; Subkey: "Units.DataFile\Shell\Open\Command"; ValueType: string; \
       ValueData: "{sys}\notepad.exe ""%1"""; Tasks: "fileassociation"; Flags: uninsdeletekey
Root: "HKCR"; Subkey: "Units.DataFile\DefaultIcon"; ValueType: string; \
      ValueData: "{app}\unitsfile.ico,0"; Flags: uninsdeletekey

; App Paths
; All users
;OnlyBelowVersion: 7.0; Root: "HKLM"; Subkey: "Software\Microsoft\Windows\CurrentVersion\App Paths\{#MyAppExeName}"; ValueType: string; ValueData: "{app}\{#MyAppExeName}"; Flags: uninsdeletekey
; Current user only
;MinVersion: 7.0; Root: "HKCU"; Subkey: "Software\Microsoft\Windows\CurrentVersion\App Paths\{#MyAppExeName}"; ValueType: string; ValueData: "{app}\{#MyAppExeName}"; Flags: uninsdeletekey
Root: "HKLM"; Subkey: "Software\Microsoft\Windows\CurrentVersion\App Paths\{#MyAppExeName}"; \
      ValueType: string; ValueData: "{app}\{#MyAppExeName}"; Flags: uninsdeletekey

[UninstallDelete]
Type: files; Name: "{app}\{#MyAppCurrencyUpdater}"

[Code]
var
    UseWindowsTerminal: Boolean;

// if Windows Terminal is installed, ask if the user wants shortcuts that use it
procedure CurStepChanged(CurStep: TSetupStep);
var
  WindowsTerminalRegKey: string;
begin
  if CurStep = ssInstall then 
  begin
    WindowsTerminalRegKey := ExpandConstant('{#MyWindowsTerminalRegKey}');
    if RegKeyExists(HKEY_LOCAL_MACHINE, WindowsTerminalRegKey) then
    begin
        // if MsgBox('Would you like to include shortcut(s) that use Windows Terminal?', mbConfirmation, MB_YESNO) = idYES then
        UseWindowsTerminal := True
    end
  else
    UseWindowsTerminal := False;
  end;
end;

function MyCreateWindowsTerminalShortcut(): Boolean;
begin
  if UseWindowsTerminal = True then
    Result := True
  else
    Result := False;
end;

// result can be used to remove AppPath from PATH
function GetAppPath(UninstallPath: string): string;
var
  InnoAppPath: string;
  AppPath: string;
begin
  InnoAppPath := 'Inno Setup: App Path';
  AppPath := '';
  if not RegQueryStringValue(HKLM, UninstallPath, InnoAppPath, AppPath) then
    RegQueryStringValue(HKCU, UninstallPath, InnoAppPath, AppPath);
  Result := AppPath;
end;

// get the string used to uninstall units
function GetUninstallString(UninstallPath: string): string;
var
  UninstallString: string;
begin
  Result := '';
  UninstallString := '';
  if not RegQueryStringValue(HKLM, UninstallPath, 'UninstallString', UninstallString) then
    RegQueryStringValue(HKCU, UninstallPath, 'UninstallString', UninstallString);
    Result := UninstallString;
end;

// remove units directory from PATH
// based somewhat on https://stackoverflow.com/questions/35410421/inno-setup-remove-path-from-path-environment-variable-while-uninstalling-a-pro
procedure RemoveFromPath(AppPath: string);
var
  OldPath: string;
  p: integer;
begin
  if not RegQueryStringValue(HKCU, 'Environment', 'PATH', OldPath) then
    begin
      MsgBox('PATH not found', mbInformation, MB_OK);
    end
  else
    begin
      p := Pos(';' + Lowercase(AppPath) + ';', ';' + Lowercase(OldPath) + ';');
      if p = 0 then
        begin
          ;//MsgBox(Format('(RemoveFromPath): AppPath [%s] not found in PATH', [AppPath]), mbInformation, MB_OK);
        end
      else
        begin
 	        if p > 1 then p := p - 1;
          Delete(OldPath, p, Length(AppPath) + 1);
          if RegWriteStringValue(HKCU, 'Environment', 'PATH', OldPath) then
	          begin
	            //Log(AppPath + ' removed from PATH');
              ;//MsgBox(AppPath + ' removed from PATH', mbInformation, MB_OK);
	          end
	        else
	          begin
	            //Log('Error writing PATH');
              MsgBox('Error writing PATH', mbInformation, MB_OK);
	          end;
	      end;
    end;
end;

// for removing AppPath for current installation from PATH
procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
var
  UninstallKey: string;
  AppPath: string;
begin
  if CurUninstallStep = usUninstall then
  begin
    // Inno Setup appends '_is1'
    UninstallKey := ExpandConstant('{#MyUninstallPath}' + '{#MyAppId}' + '_is1');
    if not RegKeyExists(HKLM, UninstallKey) then
      begin
        MsgBox('Key ' + UninstallKey + ' not found', mbInformation, MB_OK);
        exit;
      end;
    AppPath := GetAppPath(UninstallKey);
    AppPath := RemoveQuotes(AppPath);
    if (AppPath <> '') then
      RemoveFromPath(AppPath);
  end;
end;

// files use monospaced font
procedure InitializeWizard();
begin
  WizardForm.InfoBeforeMemo.Font.Name := 'Consolas';
  WizardForm.LicenseMemo.Font.Name := 'Consolas';
end;

// check for existing 32-bit installation of units and offer to uninstall it
// based somewhat on https://stackoverflow.com/questions/11739317/how-to-detect-old-installation-and-offer-removal
function InitializeSetup(): Boolean;
var
  UninstallKey: string;
  UnInstallString: string;
  AppPath: string;
  iResultCode: integer;
  begin
    UninstallKey := ExpandConstant('{#MyOldUninstallPath}' + '{#MyOldAppId}' + '_is1');
    Result := True; 
    if RegValueExists(HKEY_LOCAL_MACHINE, UninstallKey, 'UninstallString') then
    begin
      if MsgBox('Existing 32-bit installation detected.'#10'You must uninstall it to continue;'#10'do you want to uninstall it?', mbInformation, MB_YESNO) = IDYES then
      begin
        UninstallString := GetUninstallString(UninstallKey);
        UninstallString := RemoveQuotes(UninstallString);
        AppPath := GetAppPath(UninstallKey);
        AppPath := RemoveQuotes(AppPath);
        Exec(ExpandConstant(UnInstallString), '', '', SW_SHOW, ewWaitUntilTerminated, iResultCode)
        if (iResultCode <> 0) then // uninstallation failed or user declined to uninstall
          MsgBox('Existing installation not uninstalled.'#10'Quitting Setup.', mbInformation, MB_OK)
        else
          if (AppPath <> '') then
            RemoveFromPath(AppPath);
        Result := iResultCode = 0;     
      end
    else // user declined to uninstall
      begin
        MsgBox('Existing installation not uninstalled.'#10'Quitting Setup.', mbInformation, MB_OK);
        Result := False;
      end;
    end;
  end;
 
// see if directory is already in PATH
// thanks to http://stackoverflow.com/questions/3304463/how-do-i-modify-the-path-environment-variable-when-running-an-inno-setup-install
function NeedsAddPath(AppPath: string): boolean;
var
  OldPath: string;
begin
  if not RegQueryStringValue(HKCU, 'Environment', 'PATH', OldPath)
  then begin
    Result := True;
    exit;
  end;
 
  // look for AppPath in PATH with leading and trailing semicolon
  // Pos() returns 0 if not found
  Result := Pos(';' + AppPath + ';', ';' + OldPath + ';') = 0;
end;

// thanks to http://stackoverflow.com/questions/18269270/inno-setup-forward-slashes-instead-of-back-slashes
function BackslashToSlash(Param: string): string;
begin
  Result := ExpandConstant(Param);
  StringChangeEx(Result, '\', '/', True);
end;
